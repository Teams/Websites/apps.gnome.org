#!/bin/bash

dnf install --assumeyes \
  rust cargo gcc-c++ openssl-devel gettext-devel \
  clang-devel flatpak meson glibc-all-langpacks git glib2-devel \
  cairo-gobject-devel librsvg2-devel desktop-file-utils \
  gtk-update-icon-cache appstream-devel flatpak-devel \
