use page_tools::{output, Language};

use crate::config::RedirectTarget;
use crate::{config, utils};

const FILES: [&str; 2] = ["index.html", "releases.html"];

pub fn generate(languages: &[Language]) -> color_eyre::Result<()> {
    for (source_app_id, target) in config::REDIRECTS {
        let target_path = match target {
            // Manual redirect for apps that changed app id
            RedirectTarget::AppId(target_app_id) => utils::page_id(target_app_id),
            // Manual redirect for apps removed from a.g.o
            RedirectTarget::Url(url) => url.to_string(),
        };

        let src_path_full = format!("app/{source_app_id}");
        let src_path_short = utils::page_id(source_app_id);

        redirect_app_url_all_languages(languages, &src_path_full, &target_path);

        // If short path (last app id segment) has also changed
        if src_path_short != target_path {
            redirect_app_url_all_languages(languages, &src_path_short, &target_path);
        }
    }

    Ok(())
}

fn redirect_app_url_all_languages(languages: &[Language], src_path: &str, target_path: &str) {
    redirect_app_urls(target_path, "", src_path, target_path);

    for lang in languages {
        redirect_app_urls(target_path, &lang.tag_ietf, src_path, target_path);
    }
}

pub fn redirect_app_urls(title: &str, lang: &str, src_path: &str, target_path: &str) {
    for file in FILES {
        let path = format!("{src_path}/{file}");

        let target_url = if target_path.contains("://") {
            target_path.to_string()
        } else {
            let target_file = if file == "index.html" { "" } else { file };
            format!("{target_path}/{target_file}")
        };

        redirect_url(title, lang, &path, &target_url);
    }
}

fn redirect_url(title: &str, lang: &str, path: &str, url: &str) {
    let base = if url.contains("://") {
        String::new()
    } else {
        config::ABSOLUTE_BASE_URL.to_string() + "/"
    };
    let html = format!("<!DOCTYPE html><title>{title}</title><meta http-equiv='refresh' content='0; url={base}{url}'>");

    output::write_l10n(path, lang, html).unwrap();
}
