use page_tools::output::Output;
use page_tools::{i18n, Apps, Language, Site};

use crate::generate::redirects;
use crate::pages;

pub fn generate(apps: &Apps, lang: &str) {
    let language = Language::from_ietf_tag(lang);
    i18n::set_lang(lang);

    let site = Site::new(&language);

    let index = pages::Overview { site: &site, apps };
    index.output_localized("index.html", lang);

    for app in apps.values() {
        debug!("App ({lang}): {}", app.id);

        let page_id = &app.page_id;
        let app_id = &app.id;
        let app_page = pages::App { site: &site, app };

        app_page.output_localized(format!("{page_id}/index.html"), lang);

        let releases_page = pages::Releases { site: &site, app };
        releases_page.output_localized(format!("{page_id}/releases.html"), lang);

        redirects::redirect_app_urls(&app.name, lang, &format!("app/{app_id}"), &app.page_id);
    }
}
