use color_eyre::eyre::WrapErr;

use crate::collect::AppRaw;
use crate::svg;

pub async fn generate(apps: &[AppRaw]) -> color_eyre::Result<()> {
    for dir in [
        "public.new/icons/16",
        "public.new/icons/32",
        "public.new/icons/64",
        "public.new/icons/128",
        "public.new/icons/twitter",
    ] {
        std::fs::create_dir_all(dir).wrap_err_with(|| format!("Could not create {dir}."))?;
    }

    let mut tasks = tokio::task::JoinSet::new();

    for app in apps.iter().cloned() {
        tasks.spawn(tokio::task::spawn_blocking(move || {
            for size in [64, 128] {
                svg::card(&app.id.to_string(), size, &app.brand_color_light);
            }

            svg::twitter(&app.id.to_string(), &app.brand_color_light, &app.project);

            for (scheme, shade) in [("-light", 0.), ("-dark", 1.)] {
                let path = format!("public.new/icons/symbolic/{}.svg", app.id);
                if std::path::Path::new(&path).exists() {
                    for size in [16, 32] {
                        svg::generate_icon(
                            &path,
                            size,
                            shade,
                            &format!("public.new/icons/{}/{}{}.png", size, app.id, scheme),
                        )
                    }
                }
            }
        }));
    }

    while let Some(task) = tasks.join_next().await {
        task.unwrap().unwrap();
    }

    Ok(())
}
