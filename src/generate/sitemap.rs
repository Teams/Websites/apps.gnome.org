use page_tools::Language;

use crate::collect::AppRaw;
use crate::{config, sitemap};

pub fn generate(apps: &[AppRaw], languages: &[Language]) {
    for lang in languages {
        let sitemap = sitemap::generate(apps, lang, languages);
        sitemap.write_to(&format!("public.new/sitemap_{}.xml.gz", lang.tag_ietf));
    }

    let mut sitemap_index = sitemap::SitemapIndex::new();
    for lang in languages {
        sitemap_index.push_sitemap(sitemap::Sitemap::new(&format!(
            "{}/sitemap_{}.xml.gz",
            config::ABSOLUTE_BASE_URL,
            lang.tag_ietf
        )));
    }
    sitemap_index.write_to("public.new/sitemap.xml");
}
