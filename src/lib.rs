#![allow(clippy::new_without_default)]
#![allow(clippy::format_push_string)]

#[macro_use]
extern crate lazy_regex;
#[macro_use]
extern crate tracing;

mod cli;
mod collect;
mod config;
mod context;
mod damned_lies;
mod error;
mod filters;
mod flathub;
mod generate;
mod gnomeid;
mod lists;
mod output;
mod pages;
mod sitemap;
mod svg;
mod utils;

pub use generate::generate;
