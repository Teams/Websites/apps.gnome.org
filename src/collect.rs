pub mod components;
pub mod doap;
pub mod flathub_stats;
pub mod git_repos;
pub mod git_users;
pub mod people;

use std::str::FromStr;
use std::sync::Arc;

use libappstream::prelude::*;
use page_tools::AppId;
use palette::Srgb;
use serde::{Deserialize, Serialize};

use crate::{cli, damned_lies, flathub, lists, log_warn, svg, utils};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum GnomeProject {
    CoreApps,
    CoreDeveloperTools,
    Circle,
    Incubator,
}

impl GnomeProject {
    pub fn as_list_name(self) -> String {
        match self {
            Self::CoreApps => String::from("apps"),
            Self::CoreDeveloperTools => String::from("developer-tools"),
            Self::Circle | Self::Incubator => unreachable!(),
        }
    }
}

impl ToString for GnomeProject {
    fn to_string(&self) -> String {
        match self {
            Self::CoreApps => "core",
            Self::CoreDeveloperTools => "development",
            Self::Circle => "circle",
            Self::Incubator => "incubator",
        }
        .to_string()
    }
}

#[derive(Debug, Clone)]
pub struct AppRaw {
    pub id: AppId,
    pub nightly_id: Option<AppId>,
    pub project: GnomeProject,
    pub appstream: utils::UnsafeSend<libappstream::Component>,
    pub pool: libappstream::Pool,
    pub repo: Option<git_repos::Repo>,
    pub git_project: Option<git_repos::ProjectInfo>,
    pub brand_color_light: palette::Srgb,
    pub brand_color_dark: palette::Srgb,
    pub text_color_light: palette::Srgb,
    pub text_color_dark: palette::Srgb,
    pub flathub_summary: Option<flathub::AppSummary>,
    pub damned_lies: Option<damned_lies::DamnedLiesModule>,
    pub flathub_stats: Option<flathub_stats::AppStats>,
    pub circle: Option<lists::CircleDetails>,
}

impl AppRaw {
    pub async fn new(
        app_id: AppId,
        gnome_line: GnomeProject,
        pool: libappstream::Pool,
        args: Arc<cli::Args>,
        stats: Arc<flathub_stats::Stats>,
        nightly_ids: Arc<Vec<String>>,
        circle: Option<lists::CircleDetails>,
    ) -> Option<AppRaw> {
        let appdata = pool
            .components_by_id(&app_id)
            .and_then(|x| x.index_safe(0).map(utils::UnsafeSend))
            .or_else(|| {
                pool.components_by_id(&format!("{app_id}.desktop"))
                    .and_then(|x| x.index_safe(0).map(utils::UnsafeSend))
            });

        if let Some(appdata) = appdata {
            let repo = git_repos::Repo::from_appstream(appdata.clone()).await;
            let git_project = if let Some(repo) = &repo {
                let result =
                    git_users::project_info(&git_users::ProjectId::App(app_id.clone()), repo, args)
                        .await;
                if let Err(err) = &result {
                    log_warn!(app_id, "No valid .doap-file found: {}", err);
                }
                result.ok()
            } else {
                None
            };
            let id = crate::context::real_id(appdata.id().unwrap().clone());

            let mut nightly_ids = nightly_ids
                .iter()
                .filter(|x| x.starts_with(&id.to_string()))
                .map(|x| x.to_string())
                .collect::<Vec<String>>();

            nightly_ids.sort_by(|x, y| {
                if x.contains("Dev") && !y.contains("Dev") {
                    std::cmp::Ordering::Less
                } else {
                    x.cmp(y)
                }
            });

            let nightly_id = nightly_ids.first().map(AppId::new);

            let damned_lies = damned_lies::stats(&appdata, &repo).await.ok();

            let brand_color_light = appdata
                .branding()
                .and_then(|x| {
                    x.color(
                        libappstream::ColorKind::Primary,
                        libappstream::ColorSchemeKind::Light,
                    )
                    .map(|x| Srgb::from_format(Srgb::from_str(&x).unwrap()))
                })
                .unwrap_or_else(|| {
                    appdata
                        .custom_value("GnomeSoftware::key-colors")
                        .map(|value| {
                            log_warn!(&app_id, "No <branding><color scheme_preference='light'> defined, only legacy GnomeSoftware::key-colors");
                             crate::utils::parse_gs_color(&value.clone()).unwrap()[0] })
                        .unwrap_or_else(|| {
                            log_warn!(&app_id, "No <branding><color scheme_preference='light'> defined");
                            svg::dominant_colors(&id)[0]
                        })
                });

            let brand_color_dark = appdata
                .branding()
                .and_then(|x| {
                    x.color(
                        libappstream::ColorKind::Primary,
                        libappstream::ColorSchemeKind::Dark,
                    )
                    .map(|x| Srgb::from_format(Srgb::from_str(&x).unwrap()))
                })
                .unwrap_or_else(|| {
                    appdata
                        .custom_value("GnomeSoftware::key-colors")
                        .map(|value| {
                            log_warn!(&app_id, "No <branding><color scheme_preference='dark'> defined, only legacy GnomeSoftware::key-colors");
                             crate::utils::parse_gs_color(&value.clone()).unwrap()[0] })
                        .unwrap_or_else(|| {
                            log_warn!(&app_id, "No <branding><color scheme_preference='light'> defined");
                            svg::dominant_colors(&id)[0]
                        })
                });

            let flathub_summary = flathub::app_summary(&id).await;

            Some(AppRaw {
                id: id.clone(),
                nightly_id,
                project: gnome_line,
                appstream: appdata.clone(),
                pool: pool.clone(),
                repo,
                git_project,
                brand_color_light,
                brand_color_dark,
                text_color_light: svg::text_color(&brand_color_light),
                text_color_dark: svg::text_color(&brand_color_dark),
                flathub_summary: flathub_summary.clone(),
                damned_lies,
                flathub_stats: flathub_summary.map(|x| stats.app_stats(&app_id, &x.build_date())), /* stats.refs.get(&app_id.to_string()).cloned(), */
                circle,
            })
        } else {
            error!("App not found: {:?}", app_id);
            None
        }
    }

    /// Returns `None` if the app is not on Flathub
    pub fn flathub_verified(&self) -> Option<bool> {
        if self.on_flathub() {
            let verified = if let Some(verified) = self
                .appstream
                .custom_value("flathub::verification::verified")
            {
                verified.as_str() == "true"
            } else {
                false
            };

            if !verified {
                log_warn!(&self.id, "Not verified on Flathub");
            }

            Some(verified)
        } else {
            None
        }
    }

    pub fn on_flathub(&self) -> bool {
        self.flathub_summary.is_some()
    }

    pub fn screenshots(
        &self,
    ) -> (
        Vec<libappstream::Screenshot>,
        Option<Vec<libappstream::Screenshot>>,
    ) {
        let mut screenshots = self.appstream.screenshots_all();
        screenshots.sort_by_key(|x| x.kind() != libappstream::ScreenshotKind::Default);

        let light = screenshots
            .iter()
            .filter(|x| {
                let env = x.environment();
                env.as_ref().map(|x| x.as_str()) == Some("gnome")
                || env.as_ref().map(|x| x.as_str()) == Some("gnome:light")
            })
            .cloned()
            .collect::<Vec<_>>();
        let dark = screenshots
            .iter()
            .filter(|x| {
                let env = x.environment();
                env.as_ref().map(|x| x.as_str()) == Some("gnome:dark")
            }).cloned()
            .collect::<Vec<_>>();

        if !light.is_empty() && !dark.is_empty() {
            (light, Some(dark))
        } else {
            (screenshots, None)
        }
    }
}
