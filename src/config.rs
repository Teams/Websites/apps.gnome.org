pub const ABSOLUTE_BASE_URL: &str = "https://apps.gnome.org";
pub const GNOME_RELEASE: &str = "gnome-47";

pub const ADDITIONAL_CORE_APPS: &[&str] = &[
    "org.gnome.Settings",
    "org.gnome.Tour",
    "org.gnome.Extensions",
];

pub const ADDITIONAL_DEVELOPMENT_TOOLS: &[&str] = &[];

pub const ADDITION_INCUBATOR_APPS: &[&str] = &["org.gnome.Papers", "org.gnome.Showtime"];

pub const REDIRECTS: &[(&str, RedirectTarget)] = &[
    // Renamed while in circle
    (
        "org.gnome.PasswordSafe",
        RedirectTarget::AppId("org.gnome.World.Secrets"),
    ),
    // Renamed while in circle
    (
        "dev.geopjr.Hashbrown",
        RedirectTarget::AppId("dev.geopjr.Collision"),
    ),
    // Got removed from core for 42
    (
        "org.gnome.Screenshot",
        RedirectTarget::Url("https://gitlab.gnome.org/GNOME/gnome-screenshot"),
    ),
    // Renamed while in circle
    (
        "com.github.gi_lom.dialect",
        RedirectTarget::AppId("app.drey.Dialect"),
    ),
    // YOLO?
    (
        "org.gnome.Sysprof3",
        RedirectTarget::AppId("org.gnome.Sysprof"),
    ),
    // Apparently got an app id
    ("simple-scan", RedirectTarget::AppId("org.gnome.SimpleScan")),
    // Renamed while in circle
    (
        "com.gitlab.newsflash",
        RedirectTarget::AppId("io.gitlab.news_flash.NewsFlash"),
    ),
    // Replaced in core
    (
        "org.gnome.Cheese",
        RedirectTarget::AppId("org.gnome.Snapshot"),
    ),
    // Replaced in core
    ("org.gnome.eog", RedirectTarget::AppId("org.gnome.Loupe")),
    // Removed from Core
    (
        "org.gnome.Photos",
        RedirectTarget::Url("https://gitlab.gnome.org/GNOME/gnome-photos"),
    ),
    // Removed from Circle
    (
        "org.gustavoperedo.FontDownloader",
        RedirectTarget::Url("https://flathub.org/apps/org.gustavoperedo.FontDownloader"),
    ),
    // Removed from Circle
    (
        "com.github.geigi.cozy",
        RedirectTarget::Url("https://flathub.org/apps/com.github.geigi.cozy"),
    ),
    // Removed from Circle
    (
        "com.github.maoschanz.drawing",
        RedirectTarget::Url("https://flathub.org/apps/com.github.maoschanz.drawing"),
    ),
    // Apparently got an app id
    (
        "gnome-system-monitor",
        RedirectTarget::AppId("org.gnome.SystemMonitor"),
    ),
];

pub enum RedirectTarget {
    AppId(&'static str),
    Url(&'static str),
}
