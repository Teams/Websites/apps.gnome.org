use askama::Template;
use chrono::prelude::*;
use page_tools::{Apps, Site};

// used in templates
use crate::filters;

#[derive(Template)]
#[template(path = "error-404.html")]
pub struct Error404<'a> {
    pub site: &'a Site,
}

#[derive(Template)]
#[template(path = "overview.html")]
pub struct Overview<'a> {
    pub site: &'a Site,
    pub apps: &'a Apps,
}

#[derive(Template)]
#[template(path = "app.html")]
pub struct App<'a> {
    pub site: &'a Site,
    pub app: &'a page_tools::App,
}

#[derive(Template)]
#[template(path = "releases.html")]
pub struct Releases<'a> {
    pub site: &'a Site,
    pub app: &'a page_tools::App,
}
