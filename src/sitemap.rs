use std::io::prelude::*;

use page_tools::Language;

/// https://www.sitemaps.org
use crate::collect::AppRaw;
use crate::config::ABSOLUTE_BASE_URL;

pub struct SitemapIndex {
    sitemapindex: xmltree::Element,
}

impl SitemapIndex {
    pub fn new() -> Self {
        let mut namespace = xmltree::Namespace::empty();
        namespace.put("", "http://www.sitemaps.org/schemas/sitemap/0.9");

        let mut sitemapindex = xmltree::Element::new("sitemapindex");
        sitemapindex.namespaces = Some(namespace);

        Self { sitemapindex }
    }

    pub fn push_sitemap(&mut self, sitemap: Sitemap) {
        self.sitemapindex
            .children
            .push(xmltree::XMLNode::Element(sitemap.sitemap));
    }

    pub fn write_to(&self, path: &str) {
        self.sitemapindex
            .write_with_config(
                std::fs::File::create(path).unwrap(),
                xmltree::EmitterConfig::new().perform_indent(true),
            )
            .unwrap();
    }
}

pub struct Sitemap {
    sitemap: xmltree::Element,
}

impl Sitemap {
    pub fn new(loc_url: &str) -> Self {
        let mut loc = xmltree::Element::new("loc");
        loc.children
            .push(xmltree::XMLNode::Text(loc_url.to_string()));

        let mut sitemap = xmltree::Element::new("sitemap");
        sitemap.children.push(xmltree::XMLNode::Element(loc));

        Self { sitemap }
    }
}

pub struct SitemapFile {
    urlset: xmltree::Element,
}

impl SitemapFile {
    pub fn new() -> Self {
        let mut namespace = xmltree::Namespace::empty();
        namespace.put("", "http://www.sitemaps.org/schemas/sitemap/0.9");
        namespace.put("image", "http://www.google.com/schemas/sitemap-image/1.1");
        namespace.put("xhtml", "http://www.w3.org/1999/xhtml");

        let mut urlset = xmltree::Element::new("urlset");
        urlset.namespaces = Some(namespace);

        Self { urlset }
    }

    pub fn push_url(&mut self, url: Url) {
        self.urlset
            .children
            .push(xmltree::XMLNode::Element(url.url));
    }

    pub fn write_to(&self, path: &str) {
        let mut buf = Vec::new();
        self.urlset
            .write_with_config(&mut buf, xmltree::EmitterConfig::new().perform_indent(true))
            .unwrap();

        let mut encoder = flate2::write::GzEncoder::new(Vec::new(), flate2::Compression::default());
        encoder.write_all(&buf).unwrap();

        std::fs::File::create(path)
            .unwrap()
            .write_all(&encoder.finish().unwrap())
            .unwrap();
    }
}

pub struct Url {
    url: xmltree::Element,
}

impl Url {
    pub fn new(loc_url: &str) -> Self {
        let mut loc = xmltree::Element::new("loc");
        loc.children
            .push(xmltree::XMLNode::Text(loc_url.to_string()));

        let mut url = xmltree::Element::new("url");
        url.children.push(xmltree::XMLNode::Element(loc));

        Self { url }
    }

    pub fn add_link(&mut self, lang: &str, url: &str) {
        let mut link = xmltree::Element::new("link");

        link.prefix = Some(String::from("xhtml"));

        link.attributes
            .insert(String::from("rel"), String::from("alternate"));
        link.attributes
            .insert(String::from("hreflang"), lang.to_string());
        link.attributes
            .insert(String::from("href"), url.to_string());

        self.url.children.push(xmltree::XMLNode::Element(link));
    }
}

fn lang_part(language: &Language) -> String {
    let lang = &language.tag_ietf;
    if lang == "en" {
        String::new()
    } else {
        format!("{lang}/")
    }
}

pub fn generate(
    apps: &[AppRaw],
    current_language: &Language,
    all_languages: &[Language],
) -> SitemapFile {
    let mut sitemap = SitemapFile::new();

    // Overview
    let mut url = Url::new(&format!(
        "{}/{}",
        ABSOLUTE_BASE_URL,
        lang_part(current_language)
    ));

    for lang in all_languages {
        url.add_link(
            &lang.tag_ietf,
            &format!("{}/{}", ABSOLUTE_BASE_URL, lang_part(lang)),
        );
    }

    sitemap.push_url(url);

    // Apps
    for app in apps {
        let page_id = crate::utils::page_id(&app.id);

        let mut url = Url::new(&format!(
            "{ABSOLUTE_BASE_URL}/{}{page_id}/",
            lang_part(current_language),
        ));

        for lang in all_languages {
            url.add_link(
                &lang.tag_ietf,
                &format!("{ABSOLUTE_BASE_URL}/{}{page_id}/", lang_part(lang)),
            );
        }

        sitemap.push_url(url);
    }

    sitemap
}
