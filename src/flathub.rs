use std::collections::BTreeMap;

use chrono::Utc;
use color_eyre::eyre::WrapErr;
use flatpak::prelude::*;
use gio::glib;
use page_tools::AppId;
use serde::Deserialize;
use flatpak::gio;

#[derive(Debug, Clone, Deserialize)]
pub struct AppSummary {
    #[serde(rename = "timestamp")]
    pub build_time: i64,
    pub metadata: Metadata,
}

impl AppSummary {
    pub fn runtime(&self) -> String {
        self.metadata
            .runtime
            .splitn(3, '/')
            .nth(2)
            .unwrap()
            .to_string()
    }

    pub fn build_date(&self) -> chrono::NaiveDate {
        self.build_datetime().date_naive()
    }

    pub fn build_datetime(&self) -> chrono::DateTime<Utc> {
        chrono::DateTime::from_timestamp(self.build_time, 0).unwrap()
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct Metadata {
    pub runtime: String,
    pub permissions: BTreeMap<String, serde_json::Value>,
}

pub async fn app_summary(app_id: &AppId) -> Option<AppSummary> {
    let summary_response = reqwest::get(format!("https://flathub.org/api/v2/summary/{app_id}",))
        .await
        .unwrap();

    if summary_response.status().is_success() {
        Some(summary_response.json().await.unwrap())
    } else {
        debug!("App not on flathub: {}", app_id.to_string());
        None
    }
}

pub fn installation() -> color_eyre::Result<flatpak::Installation> {
    let path = gio::File::for_path("./flathub-installation");
    let installation = flatpak::Installation::for_path(&path, true, gio::Cancellable::NONE)
        .wrap_err_with(|| format!("Could not get flatpak installation for {}.", path.uri()))?;

    for (repo, name) in [
        ("data/flathub.flatpakrepo", "afg-flathub"),
        ("data/gnome-nightly.flatpakrepo", "afg-gnome-nightly"),
    ] {
        let file_content =
            std::fs::read(repo).wrap_err_with(|| format!("Could not read {repo}."))?;
        let remote = flatpak::Remote::from_file(name, &glib::Bytes::from_owned(file_content))
            .wrap_err_with(|| format!("Could not get remote {name} from {repo}."))?;
        installation
            .add_remote(&remote, true, gio::Cancellable::NONE)
            .wrap_err_with(|| format!("Could not add {repo}."))?;
    }

    Ok(installation)
}

pub fn install_data(apps: &[&AppId]) -> color_eyre::Result<()> {
    let installation = installation()?;
    let transaction =
        flatpak::Transaction::for_installation(&installation, gio::Cancellable::NONE)?;
    transaction.set_disable_dependencies(true);
    transaction.set_disable_related(true);

    for app in apps {
        if let Err(err) = transaction.add_install(
            "afg-flathub",
            &format!(
                "app/{}/{}/stable",
                app,
                flatpak::functions::default_arch().unwrap()
            ),
            &[
                &format!("/share/icons/hicolor/scalable/apps/{app}.svg"),
                &format!("/share/icons/hicolor/symbolic/apps/{app}-symbolic.svg"),
            ],
        ) {
            debug!("Cannot download app data: {}", err);
        }
    }

    transaction.run(gio::Cancellable::NONE)?;

    Ok(())
}
