use tracing_error::ErrorLayer;
use tracing_log::LogTracer;
use tracing_subscriber::prelude::*;
use tracing_subscriber::{fmt, EnvFilter, Registry};

fn main() -> color_eyre::Result<()> {
    tracing_install();
    color_eyre::install()?;
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()?
        .block_on(apps_gnome_org::generate())
}

pub fn tracing_install() {
    LogTracer::init().expect("Failed to set logger");
    let fmt_layer = fmt::layer().without_time().with_ansi(true);
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("apps_gnome_org=info"))
        .unwrap();

    // Create a file for logging and use it as a writer
    let log_file = std::fs::File::create("apps.gnome.org.log").expect("Failed to create log file");
    let file_layer = fmt::layer()
        .without_time()
        .with_ansi(false)
        .with_writer(std::sync::Mutex::new(log_file));

    let subscriber = Registry::default()
        .with(filter_layer)
        .with(fmt_layer)
        .with(ErrorLayer::default())
        .with(file_layer);
    tracing::subscriber::set_global_default(subscriber).expect("Failed to set subscriber");
}
