use askama::Template;

use crate::Language;

#[derive(Template)]
#[template(path = "languages.js", escape = "none")]
pub struct LanguagesJs {
    pub languages: Vec<Language>,
}

impl LanguagesJs {
    pub fn new(languages: Vec<Language>) -> Self {
        Self { languages }
    }
}
