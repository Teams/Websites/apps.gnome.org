use crate::Language;

pub struct Site {
    pub base_url: String,
    pub absolute_base_url: String,
    pub lang_base_url: String,
    pub assets_url: String,
    pub document_language: String,
    pub lang: Language,
    pub now_localized: String,
}

impl Site {
    pub fn new(lang: &Language) -> Self {
        let base_url = std::env::var("BASE_URL").unwrap_or_default();

        let mut lang_base_url = base_url.clone();
        lang_base_url.push_str(&format!("/{}", lang.tag_ietf));

        let assets_url = format!("{base_url}/assets");
        let document_language = lang.tag_ietf.clone();
        let now_localized = crate::i18n::datetime(&chrono::Utc::now(), lang);
        let absolute_base_url = std::env::var("ABSOLUTE_BASE_URL").unwrap_or_default();

        Self {
            base_url,
            lang_base_url,
            assets_url,
            document_language,
            lang: lang.clone(),
            now_localized,
            absolute_base_url,
        }
    }
}
