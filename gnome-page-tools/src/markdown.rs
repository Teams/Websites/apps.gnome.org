use std::path::Path;

use comrak::arena_tree::Node;
use comrak::nodes::{Ast, NodeLink, NodeValue};

use crate::i18n;

/// Path argument for translation info
pub fn template_to_html(
    template: &impl askama::Template,
    path: impl AsRef<Path>,
) -> color_eyre::Result<String> {
    let r = md_to_html(&template.render()?, path)?;
    Ok(r)
}

pub fn md_to_html(md: &str, path: impl AsRef<Path>) -> Result<String, std::io::Error> {
    let mut options = comrak::ComrakOptions::default();
    options.render.unsafe_ = true;
    options.parse.smart = true;

    let arena = comrak::Arena::new();
    let node = comrak::parse_document(&arena, md, &options);

    translate_nodes(node, &arena, &options, path.as_ref())?;

    let mut md = Vec::new();

    comrak::format_html(node, &options, &mut md)?;

    Ok(String::from_utf8_lossy(&md).to_string())
}

fn translate_nodes<'a>(
    node: &'a comrak::nodes::AstNode<'a>,
    arena: &'a comrak::Arena<comrak::nodes::AstNode<'a>>,
    options: &comrak::ComrakOptions,
    path: &Path,
) -> Result<(), std::io::Error> {
    if let comrak::nodes::NodeValue::Paragraph | comrak::nodes::NodeValue::Heading(_) =
        node.data.borrow().value
    {
        let mut md = Vec::new();

        if let comrak::nodes::NodeValue::Heading(_) = node.data.borrow().value {
            for c in node.children() {
                comrak::format_commonmark(c, options, &mut md)?;
                c.detach();
                md.pop();
            }
        } else {
            comrak::format_commonmark(node, options, &mut md)?;
            md.pop();
        }

        let md_string = std::str::from_utf8(&md).unwrap();

        i18n::add_string(
            md_string,
            Some(
                i18n::StringLocation::new(path)
                    .line(node.data.borrow().sourcepos.start.line as u32),
            ),
        );

        let md_translated: String = i18n::translate(md_string);

        if let comrak::nodes::NodeValue::Heading(heading) = node.data.borrow().value {
            let id: String = md
                .to_ascii_lowercase()
                .into_iter()
                .filter_map(|c| {
                    if c.is_ascii_alphabetic() {
                        Some(c as char)
                    } else if c.is_ascii_whitespace() {
                        Some(b'-' as char)
                    } else {
                        None
                    }
                })
                .collect();

            let node_h_content = comrak::parse_document(arena, &md_translated, options)
                .first_child()
                .unwrap();
            node_h_content.data.borrow_mut().value = comrak::nodes::NodeValue::Link(NodeLink {
                url: format!("#{id}"),
                title: String::new(),
            });

            let level = heading.level;

            let h_open = format!(r#"<h{level} id="{id}">"#);
            let h_close = format!("</h{level}>");

            let node_h_open = arena.alloc(Node::new(
                Ast::new(
                    NodeValue::HtmlInline(h_open),
                    node.data.borrow().sourcepos.start,
                )
                .into(),
            ));

            let node_h_close = arena.alloc(Node::new(
                Ast::new(
                    NodeValue::HtmlInline(h_close),
                    node.data.borrow().sourcepos.start,
                )
                .into(),
            ));

            node.insert_before(node_h_open);
            node.insert_before(node_h_content);
            node.insert_before(node_h_close);
            node.detach();
        } else {
            let new = comrak::parse_document(arena, &md_translated, options);
            node.insert_after(new);
            node.detach();
        }
    }

    for c in node.children() {
        translate_nodes(c, arena, options, path)?;
    }

    Ok(())
}
