use std::path::{Path, PathBuf};

use askama::Template;
use color_eyre::eyre::Context;

pub const TMP_DIR: &str = "public.new";
pub const TARGET_DIR: &str = "public";

pub trait Output {
    fn output_localized(&self, path: impl AsRef<Path>, lang: &str);
    fn output(&self, path: impl AsRef<Path>);
}

pub trait OutputSimple {
    fn output(&self, path: impl AsRef<Path>);
}

impl<T: Template> Output for T {
    fn output_localized(&self, path: impl AsRef<Path>, lang: &str) {
        let content = self.render().unwrap();

        if path.as_ref().extension().unwrap_or_default() == "html" {
            let binary =
                minify_html::minify(content.as_bytes(), &minify_html::Cfg::spec_compliant());
            write_l10n(path, lang, binary).unwrap();
        } else {
            write_l10n(path, lang, content).unwrap();
        }
    }

    fn output(&self, path: impl AsRef<Path>) {
        let content = self.render().unwrap();

        if path.as_ref().extension().unwrap_or_default() == "html" {
            let binary =
                minify_html::minify(content.as_bytes(), &minify_html::Cfg::spec_compliant());
            write(path, binary).unwrap();
        } else {
            write(path, content).unwrap();
        }
    }
}

pub struct Dir(PathBuf);

impl<T: AsRef<Path>> From<T> for Dir {
    fn from(path: T) -> Self {
        Self(path.as_ref().to_path_buf())
    }
}

impl OutputSimple for Dir {
    fn output(&self, path: impl AsRef<Path>) {
        let mut tmp_dest = PathBuf::from(TMP_DIR);
        tmp_dest.push(path);

        std::fs::create_dir_all(&tmp_dest).unwrap();

        fs_extra::dir::copy(&self.0, tmp_dest.parent().unwrap(), &Default::default()).unwrap();
    }
}

pub struct File(PathBuf);

impl<T: AsRef<Path>> From<T> for File {
    fn from(path: T) -> Self {
        Self(path.as_ref().to_path_buf())
    }
}

impl OutputSimple for File {
    fn output(&self, path: impl AsRef<Path>) {
        let mut tmp_dest = PathBuf::from(TMP_DIR);
        tmp_dest.push(path);

        std::fs::create_dir_all(tmp_dest.parent().unwrap()).unwrap();

        std::fs::copy(&self.0, tmp_dest).unwrap();
    }
}

pub struct Scss(PathBuf);
impl<T: AsRef<Path>> From<T> for Scss {
    fn from(path: T) -> Self {
        Self(path.as_ref().to_path_buf())
    }
}
impl OutputSimple for Scss {
    fn output(&self, path: impl AsRef<Path>) {
        let content = grass::from_path(
            &self.0,
            &grass::Options::default().style(grass::OutputStyle::Compressed),
        )
        .unwrap();
        write(path, content).unwrap();
    }
}

impl OutputSimple for String {
    fn output(&self, path: impl AsRef<Path>) {
        write(path, self).unwrap();
    }
}

pub fn prepare() {
    let _ignore = std::fs::remove_dir_all(TMP_DIR);
}

pub fn complete() -> color_eyre::Result<()> {
    let _ignore = std::fs::remove_dir_all(TARGET_DIR);
    std::fs::rename(TMP_DIR, TARGET_DIR)
        .wrap_err_with(|| format!("Could not rename {TMP_DIR} to {TARGET_DIR}"))?;
    Ok(())
}

pub fn write(path: impl AsRef<Path>, content: impl AsRef<[u8]>) -> color_eyre::Result<()> {
    let mut tmp_path = PathBuf::from(TMP_DIR);
    tmp_path.push(path);

    std::fs::create_dir_all(tmp_path.parent().unwrap())?;

    std::fs::write(tmp_path, &content)?;

    Ok(())
}

pub fn write_l10n(
    path: impl AsRef<Path>,
    lang: &str,
    content: impl AsRef<[u8]>,
) -> color_eyre::Result<()> {
    let mut tmp_path = PathBuf::from(TMP_DIR);
    if lang != "en" {
        tmp_path.push(lang.replace('_', "-"));
    }
    tmp_path.push(&path);

    std::fs::create_dir_all(tmp_path.parent().unwrap())?;
    std::fs::write(&tmp_path, &content).context(tmp_path.display().to_string())?;

    if lang == "en" {
        let mut tmp_path_2 = PathBuf::from(TMP_DIR);
        tmp_path_2.push("en");
        tmp_path_2.push(&path);

        std::fs::create_dir_all(tmp_path_2.parent().unwrap())?;

        let relative_path = pathdiff::diff_paths(&tmp_path, tmp_path_2.parent().unwrap()).unwrap();
        std::os::unix::fs::symlink(relative_path, &tmp_path_2)
            .context(tmp_path_2.display().to_string())?;
    }

    Ok(())
}
