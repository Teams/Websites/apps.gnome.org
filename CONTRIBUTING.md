# Contributing to Apps for GNOME

Contributions of all kind and with all levels of experience are very welcome. Please note that the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) applies to this project.

The translation of Apps for GNOME is managed by the [GNOME Translation Project](https://wiki.gnome.org/TranslationProject) and the respective [language teams](https://l10n.gnome.org/teams/). The translation status is available on the [module page](https://l10n.gnome.org/module/apps-for-gnome/).

## Building the website

### Requirements

Building this page has a surprising amount of dependencies. The following list might get outdated. Please check the [.gitlab-ci.yml](.gitlab-ci.yml) if something seems to be missing.

```sh
sudo ./scripts/dnf-install.sh
```

### Build

If you want translated strings you have to run

```sh
meson setup builddir
ninja install -C builddir
```

Then, run the following command to generate the site into the "public" folder

```sh
./scripts/download-non-flatpaked.py
cargo run
```

Set the `RUST_LOG` environment variable to change the log level of `apps.gnome.org`.

```sh
RUST_LOG=apps_gnome_org=debug cargo run
```

Next to the console, the log can also be found in the file `apps.gnome.org.log`.


### Developing

You can browse the website by firing up any webserver. For example like this

```sh
python3 -m http.server -d public
```

Apps for GNOME exposes a set of command line options.
You can find all of them by running:

```sh
cargo run -- --help
```

Especially `--dev` is useful, since that way you skip localization and generate fewer apps:

```sh
cargo run -- --dev
```

For moments in which you are only tweaking CSS, you can also leave this running in the background

```sh
sass --watch scss/style.scss:public/assets/style.css
```
