# Metadata for “Apps for GNOME”

Here you can find some GNOME-specific extras. Additionally, you can check the annotations on the [app overview](https://sophie-h.pages.gitlab.gnome.org/app-overview/?gnome=&id=&annotations=true) to check for issues with your app.

## AppStream

The [AppStream metadata](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html) are the ones stored in the file usually having `metainfo.xml` in its name.

### Linking to Matrix

Apps for GNOME does detect Matrix links in the `contact` URLs and displays them in a specific way. Your entry should look something like this:

```xml
<url type="contact">https://matrix.to/#/#MyApp:gnome.org</url>
```

### Colors

Currently, the first entry of [`GnomeSoftware::key-colors`](https://gitlab.gnome.org/GNOME/gnome-software/-/wikis/Software-metadata#user-content-how-to-set-the-carousel-tile-background-colour) is used for the header background. There is no way to specify different colors for light and dark mode.

##### Future

There is a standardized [`<branding>`-tag](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-branding). However, several parts in the stack don't support it yet. Therefore it's currently not recommended to specify it.

### Mobile support

To get the mobile support indicator you have to add a "mobile" [`Purism::form_factor`](https://puri.sm/posts/specify-form-factors-in-your-librem-5-apps/).

##### Future

There is a standardized [`<display_length>`-tag](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-relations-display_length). However, it's currently [removed from the metadata by Flathub](https://github.com/flathub/flathub/issues/2439) and therefore not supported yet.

## DOAP

The file containing the DOAP (Description of a Project) has to be on the top level of your repository. The filename has to be the same as the project name in the repository URL plus the extension `.doap`. For example, in this repository it is [`apps.gnome.org.doap`](apps.gnome.org.doap). You can find an [example in the Wiki](https://wiki.gnome.org/Git/FAQ).

DOAP files are also used by Builder.

### Platform

This information is not actually used on “Apps for GNOME”, but for displaying the right documentation on “Welcome to GNOME.” It can also help with tracking transition processes. Currently, there are mostly two relevant combinations of entries:

```xml
  <platform>GTK 4</platform>
  <platform>Libadwaita</platform>
```

or

```xml
  <platform>GTK 3</platform>
  <platform>Libhandy</platform>
```

### Maintainers

The information about maintainers is extracted from the GitHub and GitLab accounts listed in the .doap-file. It is enough to only list one account. If multiple accounts are listed, the information is combined. The following information is extracted:

- Name
- Profile picture
- Location
- Pronouns (only GitLab)
- GitHub and GitLab accounts
- Mastodon accounts (only GitHub)
- Websites

```xml
  <maintainer>
    <foaf:Person>
      <foaf:name>Marge Bot</foaf:name>
      <foaf:mbox rdf:resource="mailto:marge-bot@gnome.org" />
      <foaf:account>
        <foaf:OnlineAccount>
          <foaf:accountServiceHomepage rdf:resource="https://gitlab.gnome.org"/>
          <foaf:accountName>marge-bot</foaf:accountName>
        </foaf:OnlineAccount>
      </foaf:account>
      <foaf:account>
        <foaf:OnlineAccount>
          <foaf:accountServiceHomepage rdf:resource="https://github.com"/>
          <foaf:accountName>marge-bot</foaf:accountName>
        </foaf:OnlineAccount>
      </foaf:account>
    </foaf:Person>
  </maintainer>
```

##### Future

The standard also supports the fields developer, helper, tester, translator, and documentor. If you want support for any of those fields, please open an issue.
